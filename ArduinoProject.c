#include <LiquidCrystal.h>
LiquidCrystal lcd(10,12,4,5,6,7);

byte guy[8] =                                                 // all the byte arrays represent the characters or blocks that'll be printed in the lcd
{
  B00100,B01110,B10101,B00100,B00100,B01010,B01010,B01010
};
byte dino[8] = 
{
  B00010,B00111,B00111,B00110,B00111,B10110,B11110,B01010
};
byte duckdino[8] =
{
  B00000,B00000,B00010,B00111,B00111,B10110,B11110,B01010
};
byte emptySpace[8] = 
{
  B00000,B00000,B00000,B00000,B00000,B00000,B00000,B00000
};
byte block[8] =
{
  B01110,B01110,B01110,B01110,B01110,B01110,B01110,B01110
};
byte duckObj[8] = 
{
  B01110,B01110,B00100,B00000,B00000,B00000,B00000,B00000
};
byte duckguy[8] = 
{
  B00000,B00000,B00100,B01110,B10101,B00100,B01010,B01010
};
byte girl[8] = 
{
  B00100,B01110,B10101,B10101,B00100,B01110,B11111,B01010
};

int slate[2][16] = {                                      // Initializes and defines the array that records what objects are where at any given time
  {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
  {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2}
};

int counter = 0;
volatile int character;
volatile int charnum;
volatile int charnum2;
volatile int jump = 2;
volatile int duck = 3;
volatile int currentPosition = 1;
volatile int ducking = 0;
volatile int jumping = 0;
volatile int jumpCount = 0;
volatile int duckCount = 0;
int gameStarted = 0;
int select = 9;
int score;
int blockcount = 0;
int level = 1;
int seconds = 500;

void setup() {
  Serial.begin(9600);
  pinMode(select, INPUT_PULLUP);
  pinMode(jump, INPUT_PULLUP);
  pinMode(duck, INPUT_PULLUP);
  lcd.begin(16,2);
  lcd.createChar(0, dino);
  lcd.createChar(1, duckdino);
  lcd.createChar(2, emptySpace);
  lcd.createChar(3, block);
  lcd.createChar(4, duckObj);
  lcd.createChar(5, guy);
  lcd.createChar(6, duckguy);
  lcd.createChar(7, girl);

  attachInterrupt(0, jumpFunc, RISING);
  attachInterrupt(1, duckFunc, RISING);
}

void loop() {
  //Serial.println(gameStarted);
  if(gameStarted==0){
    chooseChar();
    score = 0;
    level = 0;
  }
  if(gameStarted==1){
    counter++;
    if(counter%2==0){
      for(int j = 0; j<15; j++){
          slate[0][j] = slate[0][j+1];
          slate[1][j] = slate[1][j+1];
      }
      NewSpace();
    }
    if(slate[0][1] == 3 && currentPosition == 0){
      endGame();
      
    }else if(slate[1][1] == 3 && currentPosition == 1){
      endGame();
      
    }else if(slate[1][1] == 4 && ducking == 0 && currentPosition == 1){
      endGame();
    }
    
    if(slate[1][0] != 2){
      blockcount++;
      score = blockcount/2;
      newLevel();
    }
    
    newDisp();
    lcd.setCursor(1,currentPosition);
    lcd.write(character);
  }
  delay(150);
}

void chooseChar(){
  Serial.println(gameStarted);
  while(digitalRead(select)==HIGH){  // while the enter button is not pressed display the characters and let the player choose with a potentiometer
    clearSlate();
    slate[1][4] = 5;
    slate[1][7] = 0;
    slate[1][10] = 7;
    newDisp();
    int pot = analogRead(A0);
    
    if(pot>=0 && pot<=341){
      charnum = 5;
      charnum2 = 6;
      slate[0][4] = 4;
      slate[0][7] = 2;
      slate[0][10] = 2;
      newDisp();
    
    }else if(pot>341 && pot<=682){
      charnum = 0;
      charnum2 = 1;
      slate[0][4] = 2;
      slate[0][7] = 4;
      slate[0][10] = 2;
      newDisp();
   }else if(pot>682 && pot<= 1023){
      charnum = 7;
      charnum2 = 6;
      slate[0][4] = 2;
      slate [0][7] = 2;
      slate[0][10] = 4;
      newDisp();
   }
  }
  gameStarted=1; // once they choose a character reset all values and say the game has started
  clearSlate();  
  newDisp();
  lcd.setCursor(1,1); 
  lcd.write(charnum);
  counter = 0;
  score = 0;
  level = 0;
}

void endGame(){
  gameStarted = 0;
  currentPosition = 1;
  clearSlate();
  newDisp();
  lcd.setCursor(3,0);
  lcd.print("Score: ");
  lcd.print(score, DEC);
  lcd.setCursor(3,1);
  lcd.print("Level: ");
  lcd.print(level,DEC);
  score = 0;
  level = 1;
  seconds = 500;
  delay(5000);
}

void NewSpace(){
  if(counter%8 == 0){
    int randSpace = random(1,4);
    if(randSpace==1){
      slate[0][15] = 2;
      slate[1][15] = 2;
    }else if(randSpace == 2){
      slate[0][15] = 2;
      slate[1][15] = 3;
    }else if(randSpace == 3){
      slate[0][15] = 3;
      slate[1][15] = 4;
    }
  }else{
    slate[0][15] = 2;
    slate[1][15] = 2;
  }
}

void newDisp(){
  for(int i = 0; i<=1; i++){
    for(int j = 15; j>=0; j--){
      lcd.setCursor(j,i);
      lcd.write(slate[i][j]);
    }
  }
}

void clearSlate(){
  for(int i = 0; i<2; i++){
    for(int j = 15; j>=0; j--){
      slate[i][j] = 2;
      //lcd.setCursor(j,i);
     // lcd.write(2);
    }
  }
}

void jumpFunc(){
  jumpCount++;
  duckCount = 0;
  if(jumpCount%2==1){
    currentPosition = 0;
    jumping = 1;
    ducking = 0;
    character = charnum;
  }else if(jumpCount%2 == 0){
    currentPosition = 1;
    jumping = 0;
    ducking = 0;
    character = charnum;
  }
}

void duckFunc(){
  duckCount++;
  jumpCount = 0;
  if(duckCount%2==1){
    currentPosition = 1;
    jumping = 0;
    ducking = 1;
    character = charnum2;
  }else if(duckCount%2==0){
    currentPosition = 1;
    jumping = 0;
    ducking = 0;
    character = charnum;
  } 
}

void newLevel(){
  if(score == 10 || score == 25 || score == 45 || score == 70 || score == 100 || score == 135 || score == 200){
    tone(8, 1000, 300);
    level++;
    seconds = seconds - 50;
  }
}